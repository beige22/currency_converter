import json
import re
import speech_recognition as sr
import requests as req
import pyttsx3

currency_mappings = {
    'euro': 'EUR',
    'dollars': 'USD'
}


def make_request(url, headers):
    res = req.get(url, headers=headers).text
    return res


# Command functions
def convert_currency():
    if from_currency == to_currency:
        rate = 1
    else:
        response = json.loads(
            make_request('https://api.exchangeratesapi.io/latest?base=' + from_currency + '&symbols=' + to_currency,
                         ''))
        rate = response['rates'][to_currency]

    converted_amount = round(amount * rate, 2)
    print('\n' + str(amount) + ' ' + from_currency + ' is ' + str(converted_amount) + ' ' + to_currency)


def get_weather(city):
    # mail = dopixef293@lanelofte.com
    api_key = 'c33f45439b3da9f2cf8bdf7db91c7e97'
    response = json.loads(make_request('https://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric&appid=' + api_key, ''))
    # print(response)
    speak('Current conditions: ' + response['weather'][0]['description'])
    speak('Temperature: ' + str(response['main']['feels_like']) + ' Celsius degrees')


def get_joke():
    headers = {'accept': 'application/json'}
    response = json.loads(make_request('https://icanhazdadjoke.com', headers))
    speak(response['joke'])


# Auxiliary functions
def get_available_currencies():
    response = json.loads(make_request('https://api.exchangeratesapi.io/latest', ''))
    currencies = list(response['rates'].keys())
    currencies.append('EUR')
    currencies.sort()
    return currencies


# def get_amount():
#     amount = input('Enter amount: ')
#
#     while amount.replace('.', '', 1).isdigit() == False:
#         print('Please enter a number')
#         amount = input('Enter amount: ')
#
#     return float(amount)
#
#
# def get_from_currency():
#     from_currency = input('Enter from currency: ').upper()
#
#     while from_currency not in available_currencies:
#         print('Please select currency in list')
#         from_currency = input('Enter from currency: ').upper()
#
#     return from_currency
#
#
# def get_to_currency():
#     to_currency = input('Enter to currency: ').upper()
#
#     while to_currency not in available_currencies:
#         print('Please select currency in list')
#         to_currency = input('Enter to currency: ').upper()
#
#     return to_currency.upper()


# Voice functions
def get_voice_input():
    with microphone as source:
        recognizer.adjust_for_ambient_noise(source)
        # print('\n' + help_message)
        speak(help_message)
        audio = recognizer.listen(source)

    recognizer_response = {
        'success': True,
        'error': None,
        'transcription': None
    }

    try:
        recognizer_response['transcription'] = recognizer.recognize_google(audio)
    except sr.UnknownValueError:
        recognizer_response['success'] = False
        recognizer_response['error'] = 'Unable to recognize speech'
    except sr.RequestError:
        recognizer_response['success'] = False
        recognizer_response['error'] = 'API unavailable'

    return recognizer_response


def get_transcription():
    response = get_voice_input()

    while not response['success']:
        print('[ERROR] ' + response['error'])

        if response['error'] == 'Unable to recognize speech':
            print('Please speak again')
            response = get_voice_input()
        else:
            quit(1)

    transcription = response['transcription']
    print('Transcription: ' + transcription)

    return transcription


def speak(text):
    engine.say(text)
    engine.runAndWait()


# Command functions
def get_command():
    print('\nAvailable commands: ' + available_commands.__str__())
    transcription = get_transcription()

    if transcription in available_commands:
        if 'exit' in transcription:
            exit(0)
        return transcription
    else:
        return 'none'


def get_command_params():
    transcription = get_transcription()
    if 'exit' in transcription:
        exit(0)
    return transcription


if __name__ == '__main__':
    available_currencies = get_available_currencies()
    available_commands = ['convert currency', 'joke', 'weather', 'exit']

    currency_regex = r"[0-9]*\.*[0-9]*.[a-z]{3}.to.[a-z]{3}"
    pattern = re.compile(currency_regex, re.IGNORECASE)

    recognizer = sr.Recognizer()
    microphone = sr.Microphone()

    engine = pyttsx3.init()

    while True:
        help_message = 'Please say a command'
        command = get_command()

        if command == 'convert currency':
            help_message = 'Please tell amount and two available currencies'
            print('Available currencies:\n' + str(available_currencies) + '\n')
            params = get_command_params().replace('.', ' ')

            while pattern.match(params) is None or params.split(' ')[1].upper() not in available_currencies or \
                    params.split(' ')[3].upper() not in available_currencies:
                params = get_command_params()

            params = params.split(' ')
            amount = float(params[0])
            from_currency = params[1].upper()
            to_currency = params[3].upper()

            convert_currency()

        elif command == 'weather':
            help_message = 'Please say a city'
            params = get_command_params()
            get_weather(params)

        elif command == 'joke':
            get_joke()
